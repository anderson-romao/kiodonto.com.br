$('.mostrar-submenu').click(function () {
    $('html, body').animate({scrollTop:$(document).height()}, 'slow');
    $('.dropdown-menu').toggle();
    return false;
});

// Home -> profissionais
$(document).ready(function(){
	$('.carousel-profissionais').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/left-arrow-gray.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/right-arrow-gray.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1024,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 3
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});
});

// Home -> depoimentos
$(document).ready(function(){
	$('.carousel-depoimentos').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 5,
		autoplay: true,
		autoplaySpeed: 3000,
		arrows: true,
		dots: true,
		prevArrow:'<img src="imagens/logos/left-arrow-gray.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/right-arrow-gray.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				pauseOnHover: true,
			}
		}, {
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 2,
				pauseOnHover: true,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				pauseOnHover: true,
				dots: true,
			}
		}
		]
	});
});

//Iniciar animações
AOS.init({
	disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});